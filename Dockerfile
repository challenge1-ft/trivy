# Imagen de ubuntu 20.04
FROM ubuntu:20.04

#Directorio de trabajo 
WORKDIR /src

# Descarga de trivy dentro del directorio
ADD https://github.com/aquasecurity/trivy/releases/download/v0.32.0/trivy_0.32.0_Linux-64bit.tar.gz .

# Se mueve lo descargado al bin y se le da los permisos necesarios
RUN mv ./trivy_0.32.0_Linux-64bit.tar.gz /usr/local/bin/trivy \
&& chmod +x /usr/local/bin/trivy

# ENTRYPOINT de ubuntu.
ENTRYPOINT [""]
